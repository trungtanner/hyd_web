var viewer = (function () {
    var email;
    var displayName;
    var token;
    var uid;

    const code = {
        "email":"email",
        "displayName":"displayName",
        "token":"token",
        "uid":"uid"
    }

    const register = function (id, view) {
        switch (view) {
            case code.email:
                email = document.getElementById(id);
                break;
            case code.displayName:
                displayName = document.getElementById(id);
                break;
            case code.token:
                token = document.getElementById(id);
                break;
            case code.uid:
                uid = document.getElementById(id);
                break;
            default:
                break;
        }
    }

    const registerArray = function (ids, views) {
        for(let i = 0; i < views.length; i++){
            register(ids[i],views[i]);
        }
    }

    const updateEmail = function(value){
        if (email){
            email.innerHTML = value;
        }
    }

    const updateDisplayName = function(value){
        if (displayName){
            displayName.innerHTML = value;
        }
    }

    const updateUid = function(value){
        if (uid){
            uid.innerHTML = value;
        }
    }

    const updateToken = function(value){
        if (token){
            token.innerHTML = value;
        }
    }

    const updateWithJson = function(json){
        if (json["email"] && email){
            email.innerHTML = json["email"];
        }

        if (json["token"] && token){
            token.innerHTML = json["token"];
        }

        if(json["displayName"] && displayName){
            displayName.innerHTML = json["displayName"];
        }

        if(json["uid"] && uid){
            uid.innerHTML = json["uid"];
        }
    }

    //public methods
    return {
        code:code,
        register:register,
        registerArray:registerArray,
        updateEmail:updateEmail,
        updateWithJson:updateWithJson
    }
})();

export { viewer };