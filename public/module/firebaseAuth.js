import { viewer } from "../module/viewer.js";

var firebaseAuth = (function () {
  const firebaseConfig = {
    apiKey: "AIzaSyDjfyYj6A2cB5pGrekCxTFjQfQJBLkjIAA",
    authDomain: "clicker-tanner.firebaseapp.com",
    databaseURL: "https://clicker-tanner-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "clicker-tanner",
    storageBucket: "clicker-tanner.appspot.com",
    messagingSenderId: "99313898302",
    appId: "1:99313898302:web:685332c7e0a63c1b9a126f",
    measurementId: "G-GKW25FCGFE"
  };

  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const auth = firebase.auth();

  // Signup function
  const signUp = function (successCallback) {
    var email = document.getElementById("email");
    var password = document.getElementById("password");

    const promise = auth.createUserWithEmailAndPassword(
      email.value,
      password.value
    ).then(result => {
      successCallback(result);
    });
    promise.catch((e) => alert(e.message));
  }

  // SignIN function
  const signIn = function (successCallback) {
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    const promise = auth.signInWithEmailAndPassword(
      email.value, password.value).then(result => {
          successCallback(result);
      })

    promise.catch((e) => alert(e.message));
  }

  const signInWithCustomToken =  function (successCallback) {
    var customToken = document.getElementById("custom_token");
    const promise = auth.signInWithCustomToken(customToken.value).then(result => {
          successCallback(result);
      })

    promise.catch((e) => alert(e.message));
  }


  // SignOut
  const signOut = function (successCallback) {
    auth.signOut().then(result => {
      successCallback(result);
    });
  }

  //getUser
  const getUser = function () {
    return auth.currentUser;
  }

  // Active user to homepage
  auth.onAuthStateChanged((user) => {
    if (user) {
      user.getIdToken().then(token => {
        let json = {}
        json[viewer.code.token] = token
        viewer.updateWithJson(json)
      })

      alert("Active user: " + user.email);
      let json = {};
      json[viewer.code.email] = user.email;
      json[viewer.code.displayName] = user.displayName;
      json[viewer.code.uid] = user.uid
      viewer.updateWithJson(json)

    } else {
      alert("No Active user Found");
    }
  });

  //public methods
  return {
    signUp: signUp,
    signIn: signIn,
    signInWithCustomToken:signInWithCustomToken,
    signOut: signOut,
    getUser: getUser
  }
})();

export {firebaseAuth};