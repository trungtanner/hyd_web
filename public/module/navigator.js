var navigator = (function(){

    const navigate = function(url) {
        window.location.replace(url);
    }

    const navigateToPage = function(page) {
        const url = window.location.origin + "/" + page
        window.location.replace(url);
    }

    const getCurrentUrl = function() {
        return window.location.href;
    }

    //public methods
    return{
        navigate:navigate,
        navigateToPage: navigateToPage,
        getCurrentUrl:getCurrentUrl
    }
})();

export {navigator};