var MapBox = (function(){
    //add marker
    const popup = new mapboxgl.Popup().setHTML(
        `<h3>Trung</h3><p>Hello dude</p>`
    );

    const addMarker = function(lng,lat){
        const marker = new mapboxgl.Marker()
        .setLngLat([lng, lat])
        .setPopup(popup)
        .addTo(map);
    };

    //public methods
    return{
        addMarker:addMarker
    }
})();