import { viewer } from "../module/viewer.js";
import { firebaseAuth } from "../module/firebaseAuth.js";

viewer.registerArray([
    "emailView",
    "displayNameView",
    "tokenView",
    "uidView"
], [
    viewer.code.email,
    viewer.code.displayName,
    viewer.code.token,
    viewer.code.uid
])

var signOutButton = document.getElementById('signOut');
signOutButton.onclick = function(){
    firebaseAuth.signOut(result => {
        alert("SignOut Successfully from System");
    })
}

var getTokenButton = document.getElementById("getToken");
getTokenButton.onclick = function(){
    firebaseAuth.signInWithCustomToken(result => {
        alert("You are signing in by account: " + result.user.email);
    })
}